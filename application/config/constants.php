<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ',							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',		'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',					'ab');
define('FOPEN_READ_WRITE_CREATE',				'a+b');
define('FOPEN_WRITE_CREATE_STRICT',				'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');


/*
|--------------------------------------------------------------------------
| Application Constantes
|--------------------------------------------------------------------------
|
| 
|
*/
define('STATUS_DISABLED',					'0');
define('STATUS_ENABLED',					'1');
define('STATUS_TEMPORALY_DISABLED',			'2');

define('FORM_PROJECTID', 'projectid');
define('FORM_FORMID', 'formid');

define('SESSION_PROJECTID', 'projectid');

/*
|--------------------------------------------------------------------------
| DB Constantes
|--------------------------------------------------------------------------
|
| 
|
*/
define('DB_TABLE_LIST',					'list');
define('DB_TABLE_LISTVALUE',					'listvalue');
define('DB_TABLE_ASSOCLISTVALUE',					'assoc_list_listvalue');
define('DB_TABLE_ASSOCPROJECTLIST',					'assoc_project_list');
define('DB_TABLE_FORM',					'form');
define('DB_TABLE_COMPONENT',					'component');
define('DB_TABLE_SECTION',					'section');


define('DB_COL_ID',					'id');
define('DB_COL_LABEL',			'label');
define('DB_COL_DISABLED',			'disabled');

define('DB_COL_IDPROJECT',			'idproject');
define('DB_COL_IDLIST',			'idlist');
define('DB_COL_IDLISTVALUE',			'idlistvalue');

define('DB_COL_REQUIRED',			'required');
define('DB_COL_TYPE',			'type');
define('DB_COL_LIST',			'list');
define('DB_COL_ORDER',			'order');
define('DB_COL_ROWS',			'rows');


/* End of file constants.php */
/* Location: ./application/config/constants.php */