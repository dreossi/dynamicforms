<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class EditForm extends CI_Controller {

	// variable that holds dynamic form html
	private $html;
	private $projectId;



	public function index($param)//param: formId
	{
		
		global $projectId;
	
		global $html;
		$html = array('content' => '');
		
		$this->load->library('session');
		
		$this->load->model('DbOperation');
		
		$this->load->view('header');
		
		$projectId = $this->session->userdata(SESSION_PROJECTID);
		if($projectId == FALSE)
		{
			// TODO: valida��o caso nao exista projeto selecionado
			// display error message
			// return;
			$projectId = '1';
		}
		
		$html['content'] .= '	
		<h1 class="page-header">Edit Form</h1>
			<div class="row placeholders">
            <div class="col-sm-12">
				<nav class="navbar navbar-default">
				  <div class="container-fluid">
					<div class="navbar-header">
					  
					</div>
				  </div>
				</nav>
            </div>
			<br><br>
			</div>	
		';
		
		//TODO: set process form path and variables
		$html['content'] .= '<form data-toggle="validator" role="form" action="/projects/dynamicforms/index.php/processform/index/123/" method="post">
								<input type="hidden" name="'.FORM_FORMID.'" id="'.FORM_FORMID.'" value="'.$param.'">
								<input type="hidden" name="'.FORM_PROJECTID.'" id="'.FORM_PROJECTID.'" value="'.$projectId.'">';
	
		$pattern = '/^[1-9][0-9]*$/';
		if($param == '0') //new form
		{
			$this->create($param);
		}
		else if( preg_match($pattern, $param) )
		{
			$this->edit($param);
		}
		else
		{
			; //TODO: chama funcao que imprime que formulario nao existe
		}
		
					
				
		

		$this->load->view('editform', $html);


		$this->load->view('editformfooter');
		
	}
	
	
	private function create($param)
	{
		global $html;
		global $projectId;

		
		
		$html['content'] .= '
									<div class="row">
										<div class="col-xs-12 col-md-9">
											<div class="form-group">
												<label>Form Title</label>
												<input type="text" class="form-control" name="'.DB_TABLE_FORM.'_'.DB_COL_LABEL.'_'.$projectId.'_n001" required>
												<span class="help-block with-errors"></span>
											</div>
										</div>
										<div class="col-xs-6 col-md-3">
											<div class="form-group">
												<label>Disabled:</label><br>
												<input name="'.DB_TABLE_FORM.'_'.DB_COL_DISABLED.'_'.$projectId.'_n001" type="checkbox">
											</div>
										</div>
									</div>
										';
										
		$html['content'] .= '
							<button name="section" id="section" type="button" class="btn btn-default add-section" aria-label="Left Align">
								<span class="glyphicon glyphicon-plus-sign" aria-hidden="true"> Add Section</span>
							</button>
							<button type="submit" class="btn btn-primary">Submit</button>
						</form>';

		
	}
	
	private function edit($param) // form already exists
	{
		global $html;
		global $projectId;
		
		
		// Form Info
		$formInfo = $this->getFormInfo($param);
		/*
		echo '<br><br><br><br><br><br><br><pre>';
		print_r($formInfo);
		echo '</pre>';
		*/
		$checked = '';
		if($formInfo[0][DB_COL_DISABLED] == '1')
		{
			$checked = 'checked';
		}
		$html['content'] .= '
									<div class="row">
										<div class="col-xs-12 col-md-9">
											<div class="form-group">
												<label>Form Title</label>
												<input type="text" class="form-control" name="'.DB_TABLE_FORM.'_'.DB_COL_LABEL.'_'.$projectId.'_'.$formInfo[0][DB_COL_ID].'" value="'.$formInfo[0][DB_COL_LABEL].'" required>
												<span class="help-block with-errors"></span>
											</div>
										</div>
										<div class="col-xs-6 col-md-3">
											<div class="form-group">
												<label>Disabled:</label><br>
												<input name="'.DB_TABLE_FORM.'_'.DB_COL_DISABLED.'_'.$projectId.'_'.$formInfo[0][DB_COL_ID].'" type="checkbox" '.$checked.'>
											</div>
										</div>
									</div>
										';
		
		// retorna as se��es
		$sections = $this->getSections($param);
		/*
		echo '<br><br><br><br><br><br><br><pre>';
		print_r($sections);
		echo '</pre>';
		*/
		
		$sectionCounter = 0;
		
		foreach ($sections as $s)
		{
			$checked = '';
			if($s[DB_COL_DISABLED] == '1')
			{
				$checked = 'checked';
			}
			$sectionCounter = $sectionCounter + 1;
			
			$html['content'] .= '<!-- inicio secao -->
								 
									<div id="section' . $sectionCounter . '">
										<div class="panel panel-default">
											<div class="panel-heading">
												<h3 class="panel-title">
													<div class="row">
														<div class="col-xs-9 col-md-6">
															<div class="form-group">
																<label>Section Title</label>
																<input type="text" class="form-control" name="'.DB_TABLE_SECTION.'_'.DB_COL_LABEL.'_'.$s[DB_COL_ID].'_'.$s[DB_COL_ID].'" value="'.$s[DB_COL_LABEL].'">
															</div>
														</div>
														<div class="col-xs-6 col-md-3">
															<div class="form-group">
																<label>Section Order</label>
																<input type="text" class="form-control" name="sectionorder_'.$s[DB_COL_ID].'" value="'.$s[DB_COL_ORDER].'">
															</div>
														</div>
														<div class="col-xs-6 col-md-3">
															<div class="form-group">
																<label>Disabled:</label><br>
																<input name="disabled_'.$s[DB_COL_ID].'" type="checkbox" '.$checked.'>
															</div>
														</div>
													</div>
												</h3>
											</div>
											<div class="panel-body">';
			
										

			$fields = $this->getSectionFields($s[DB_COL_ID]);
			foreach($fields as $f)
			{	
				$generalErrorMessage = 'Check this information...';
			
				// disabled
				$disabledVar = '';
				if($f[DB_COL_DISABLED] == '1')
				{
					$disabledVar = 'checked';
				}
				
				$requiredVar = '';
				$requiredText = '';
				if($f[DB_COL_REQUIRED] == '1')
				{
					$requiredVar = 'checked';
					$requiredText = ' *';
				}
				
				$html['content'] .= '
					<div id="section' .$sectionCounter. '">
												<div class="panel panel-default">
												  <div class="panel-body">
													<div class="row">
														<div class="col-xs-6 col-md-3">
															<div class="form-group">
																<label>Field Type:</label><br>
																<div class="input-group">
																	<select name="'.DB_TABLE_COMPONENT.'_'.DB_COL_TYPE.'_'.$s[DB_COL_ID].'_'.$f[DB_COL_ID].'" class="form-control">
																		<option value="'.$f[DB_COL_TYPE].'">'.$f[DB_COL_TYPE].'</option>
																		<option value="001">Single Line Text</option>
																		<option value="002">DropDown List</option>
																		<option value="004">MultLine Text</option>
																	</select>
																</div>
															</div>
														</div>
														<div class="col-xs-6 col-md-3">
															<div class="form-group">
																<div class="input-group">
																	<label>Label:</label><br>
																	<input type="text" class="form-control" name="'.DB_TABLE_COMPONENT.'_'.DB_COL_LABEL.'_'.$s[DB_COL_ID].'_'.$f[DB_COL_ID].'" value="'.$f[DB_COL_LABEL].'">
																</div>
															</div>
															<div class="form-group">
																<div class="input-group">
																	<label>List:</label><br>
																	<select name="'.DB_TABLE_COMPONENT.'_'.DB_COL_LIST.'_'.$s[DB_COL_ID].'_'.$f[DB_COL_ID].'" class="form-control" disabled>
																		<option value="'.$f[DB_COL_LIST].'">'.$f[DB_COL_LIST].'</option>
																		<option value="001">list 1</option>
																		<option value="002">list 2</option>
																		<option value="004">list 3</option>
																	</select>
																</div>
															</div>
														</div>
														<div class="col-xs-6 col-md-3">
															<div class="form-group">
																<div class="input-group">
																	<label>Order:</label><br>
																	<input type="text" class="form-control" name="'.DB_TABLE_COMPONENT.'_'.DB_COL_ORDER.'_'.$s[DB_COL_ID].'_'.$f[DB_COL_ID].'" value="'.$f[DB_COL_ORDER].'" style="width:50%">
																</div>
															</div>
															<div class="form-group">
																<div class="input-group">
																	<label>Rows:</label><br>
																	<input type="text" class="form-control" name="'.DB_TABLE_COMPONENT.'_'.DB_COL_ROWS.'_'.$s[DB_COL_ID].'_'.$f[DB_COL_ID].'" value="'.$f[DB_COL_ROWS].'" style="width:50%" disabled>
																</div>
															</div>
														</div>
														<div class="col-xs-6 col-md-3">
															<div class="form-group">
																<div class="input-group">
																	<label>Required:</label><br>
																	<input name="'.DB_TABLE_COMPONENT.'_'.DB_COL_REQUIRED.'_'.$s[DB_COL_ID].'_'.$f[DB_COL_ID].'" type="checkbox" '.$requiredVar.'>
																</div>
															</div>
															<div class="form-group">
																<div class="input-group">
																	<label>Disabled:</label><br>
																	<input name="'.DB_TABLE_COMPONENT.'_'.DB_COL_DISABLED.'_'.$s[DB_COL_ID].'_'.$f[DB_COL_ID].'" type="checkbox" '.$disabledVar.'>
																</div>
															</div>
														</div>
													</div>
												  </div>
												</div>
									</div>
				';
			
			}
			
			$html['content'] .= '		
									<div id="section' .$sectionCounter. '">
													<button name="'.$s[DB_COL_ID].'" id="'.$s[DB_COL_ID].'" type="button" class="btn btn-default add-field" aria-label="Left Align" onclick="addField(this.id);">
													<span class="glyphicon glyphicon-plus-sign" aria-hidden="true"> Add Field</span>
													</button>
												</div>
											</div>
										</div>
									</div>
								 <!-- fim secao -->';
			
		}
		
		$html['content'] .= '
							<button name="section" id="section" type="button" class="btn btn-default add-section" aria-label="Left Align">
								<span class="glyphicon glyphicon-plus-sign" aria-hidden="true"> Add Section</span>
							</button>
							<button type="submit" class="btn btn-primary">Submit</button>
						</form>';
		
	}
	
	private function getFormInfo($formId)
	{
	
		return $this->DbOperation->getForm($formId);
		/*
		$arr = array(
						array(
							'id' => '123', 'label' => 'Formulario 1', 'disabled' => '1'
						)
						);
		return $arr;
		*/
	}
	
	private function getSections($formId)
	{
		return $this->DbOperation->getFormSections($formId);
		/*
		$arr = array(
						array(
							'id' => '123', 'label' => 'Secao 1', 'disabled' => '0', 'order' => '0', 'list' => '0', 'rows' => '0'
						),
						
						array(
							'id' => '122', 'label' => 'Secao 1.1', 'disabled' => '0', 'order' => '0', 'list' => '0', 'rows' => '0'
						),
						array(
							'id' => '124', 'label' => 'Secao 2', 'disabled' => '0', 'order' => '0', 'list' => '0', 'rows' => '0'
						)	
											
						);
		return $arr;
		*/
	}
	
	private function getSectionFields($formId)
	{
		return $this->DbOperation->getSectionFields($formId);
		/*
		return array(
						array(
							'sectionid' => '123', 'id' => $sectionId.'12344125', 'type' => '001', 'label' => 'campo de texto 1', 'required' => '1', 'tip' => 'tip', 'disabled' => '1', 'value' => 'valor do banco1', 'width' => '10', 'order' => '0', 'list' => '0', 'rows' => '0'
						),
						array(
							'sectionid' => '123', 'id' => $sectionId.'12344115', 'type' => '003', 'label' => 'campo de data 1', 'required' => '0', 'tip' => 'tip', 'disabled' => '1', 'value' => '', 'width' => '20', 'order' => '0', 'list' => '0', 'rows' => '0'
						),
						array(
							'sectionid' => '123', 'id' => $sectionId.'12344126', 'type' => '001', 'label' => 'campo de texto 11', 'required' => '1', 'tip' => 'tip', 'disabled' => '0', 'value' => 'valor da lista', 'width' => '30', 'order' => '0', 'list' => '0', 'rows' => '0'
						),
						array(
							'sectionid' => '123', 'id' => $sectionId.'12344127', 'type' => '002', 'label' => 'drop down 1', 'required' => '1', 'tip' => 'tip', 'disabled' => '1', 'value' => '', 'valuelist' => 'valor1[(&)]valor2', 'width' => '50', 'order' => '0', 'list' => '0', 'rows' => '0'
						),
						array(
							'sectionid' => '123', 'id' => $sectionId.'123441279', 'type' => '004', 'label' => 'textarea 1', 'required' => '1', 'tip' => 'tip', 'disabled' => '1', 'value' => 'campo preenchido', 'valuelist' => 'valor1[(&)]valor2', 'width' => '100', 'rows' => '10', 'order' => '0', 'list' => '0', 'rows' => '0'
						),
						array(
							'sectionid' => '124', 'id' => $sectionId.'1234', 'type' => '001', 'label' => 'campo de texto 2', 'required' => '1', 'tip' => 'tip', 'disabled' => '0', 'value' => 'valor do banco3 banco carro moto banco', 'width' => '100', 'order' => '0', 'list' => '0', 'rows' => '0'
						)						
						);
						*/
	}
}

?>