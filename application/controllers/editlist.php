<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class EditList extends CI_Controller {

	// variable that holds dynamic form html
	private $html;



	public function index($param)//param: projetoId.
	{

	
		global $html;
		$html = array('content' => '');
		
		//$this->load->database();
		
		$this->load->model('FieldName');
		
		$this->load->model('DbOperation');
		
		$this->load->view('header');
		

		
		$html['content'] .= '	
		<h1 class="page-header">Edit List</h1>
			<div class="row placeholders">
            <div class="col-sm-12">
				<nav class="navbar navbar-default">
				  <div class="container-fluid">
					<div class="navbar-header">
					  
					</div>
				  </div>
				</nav>
            </div>
			<br><br>
			</div>	
		';
		
		$html['content'] .= '<form data-toggle="validator" role="form" action="/projects/dynamicforms/index.php/processform/index/1/" method="post">
								<input type="hidden" name="'.FORM_PROJECTID.'" id="'.FORM_PROJECTID.'" value="'.$param.'">';
	
	
		$this->edit($param);
	/*
		if($param == 0) //new form
		{
			$this->create($param);
		}
		else if(1) //TODO: ver se esse form realmente existe no banco
		{
			$this->edit($param);
		}
		else
		{
			; //TODO: chama funcao que imprime que formulario nao existe
		}
		*/
		
		$html['content'] .= '
							<button name="section" id="section" type="button" class="btn btn-default add-section" aria-label="Left Align">
								<span class="glyphicon glyphicon-plus-sign" aria-hidden="true"> Add List</span>
							</button>
							<button type="submit" class="btn btn-primary">Submit</button>
						</form>';			
				
		

		$this->load->view('editlist', $html);


		$this->load->view('editlistfooter');
		
	}
	
	
	private function create($param)
	{
		global $html;

		//$asd = constant('FOPEN_READ');
		
		/*
		$html['content'] .= '
									<div class="row">
										<div class="col-xs-12 col-md-9">
											<div class="form-group">
												<label>Form Title</label>
												<input type="text" class="form-control" id="title" name="title" required>
												<span class="help-block with-errors"></span>
											</div>
										</div>
										<div class="col-xs-6 col-md-3">
											<div class="form-group">
												<label>Disabled:</label><br>
												<input name="disabled" type="checkbox">
											</div>
										</div>
									</div>
										';

		*/
	}
	
	private function edit($param) // form already exists
	{
		global $html;
		

		
		// retorna as se��es
		$sectionCounter = 0;
		$fieldsPerRow = 4;
		$sections = $this->getLists($param);
		//echo '<br><br><br><br><br><br><br><pre>';
		//print_r($sections);
		//echo '</pre>';
		foreach ($sections as $s)
		{
			$disabledVar = '';
			if($s['disabled'] == '1')
			{
				$disabledVar = 'checked';
			}
			
			$sectionCounter = $sectionCounter + 1;
			//$var = $this->generateFieldName('label', $param, $s['id']);
			$html['content'] .= '<!-- inicio secao -->
								 
									<div id="section' . $sectionCounter . '">
										<div class="panel panel-default">
											<div class="panel-heading">
												<h3 class="panel-title">
													<div class="row">
														<div class="col-xs-9 col-md-6">
															<div class="form-group">
																<label>List Title</label>
																<input type="text" class="form-control" name="'.$this->generateFieldName('list', 'label', $param, $s['id']).'" value="'.$s['label'].'">
															</div>
														</div>
														<div class="col-xs-6 col-md-3">
															<div class="form-group">
																<label>Disabled:</label><br>
																<input name="'.$this->generateFieldName('list', 'disabled', $param, $s['id']).'" type="checkbox" '.$disabledVar.'>
															</div>
														</div>
													</div>
												</h3>
											</div>
											<div class="panel-body">
												<!-- <div id="field0"> -->';
			
										
			//			
			// loop para pegar campos
			//
			$fields = $this->getValues($s['id']);
			//echo '<br><br><br><br><br><br><br><pre>';
			//print_r($sections);
			//echo '</pre>';
			$totalFields = count($fields);
			$fieldCounter = 0;
			
			if($totalFields == 0)
			{
				$html['content'] .= ' <div id="field' . $fieldCounter . '">
											<div class="row">
												</div>';
			}
			
			foreach($fields as $f)
			{	
				

				// disabled
				$disabledVar = '';
				if($f['disabled'] == '1')
				{
					$disabledVar = 'checked';
				}

				
				if($fieldCounter % $fieldsPerRow == 0)
				{
					
					$html['content'] .= ' <div id="field' . $fieldCounter . '">
											<div class="row">';
				}
				
				
				$html['content'] .= '
														<div class="col-xs-6 col-md-3">
															<div class="form-group">
																<div class="input-group">
																	<span class="input-group-addon">
																		<input type="checkbox" name="'.$this->generateFieldName('listvalue', 'disabled', $s['id'], $f['id']).'" '.$disabledVar.'>
																	</span>
																	<input type="text" class="form-control" name="'.$this->generateFieldName('listvalue', 'label', $s['id'], $f['id']).'" value="'.$f['label'].'">
																</div>
															</div>
														</div>
				';
				
				
				if($totalFields == $fieldCounter + 1)
				{
					$remainingFields = $fieldsPerRow - ($totalFields % $fieldsPerRow);
					//echo '<br><br><br><br><br>'.$totalFields.'<br>'.$fieldCounter.'<br>'.$remainingFields;
					while($remainingFields > 0)
					{
						
						$html['content'] .= '
																<div class="col-xs-6 col-md-3">
																	<div class="form-group">
																		<div class="input-group">
																			<span class="input-group-addon">
																				<input type="checkbox" name="'.$this->generateFieldName('listvalue', 'disabled', $s['id'], 'n'.$remainingFields).'">
																			</span>
																			<input type="text" class="form-control" name="'.$this->generateFieldName('listvalue', 'label', $s['id'], 'n'.$remainingFields).'">
																		</div>
																	</div>
																</div>
						';
						
						$remainingFields = $remainingFields - 1;
					}
				}
				
				if($fieldCounter > 0)
				{
					if($fieldCounter % $fieldsPerRow == 0)
					{
						$html['content'] .= '	<!-- </div> -->
												</div>';
					}
				}
				
				
				$fieldCounter = $fieldCounter + 1;
			}
			
			
			
			
			
			$html['content'] .= '		
									</div>
												<div id="section' . $sectionCounter . '">
													<button name="'. $s['id'] .'" id="'. $s['id'] .'" type="button" class="btn btn-default add-field" aria-label="Left Align" onclick="addField(this.id);">
													<span class="glyphicon glyphicon-plus-sign" aria-hidden="true"> Add Value</span>
													</button>
												</div>
											</div>
										</div>
									</div>
								 <!-- fim secao -->';
			
		}
		

		//$this->load->view('editform', $html);
		
		
		/*
			<form role="form">
			  <!-- -->
			  <div class="form-group">
				<div class="panel panel-default">
				  <div class="panel-heading">
					<h3 class="panel-title">Panel title</h3>
				  </div>
				  <div class="panel-body">
				  <!-- -->
					<label for="exampleInputEmail1">Email address</label>
					<input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
				  <!-- -->
				  </div>
				</div>
			  </div>
			  <!-- -->
			  <div class="form-group">
				<div class="panel panel-default">
				  <div class="panel-heading">
					<h3 class="panel-title">Panel title</h3>
				  </div>
				  <div class="panel-body">
				  <!-- -->
					<label for="exampleInputPassword1">Password</label>
					<input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
			  </div>
			  </div>
			  </div>
			  <div class="checkbox">
				<label>
				  <input type="checkbox"> Check me out
				</label>
			  </div>
			  <button type="submit" class="btn btn-default">Submit</button>
			</form>

		*/

		//$this->load->view('editformfooter');
	}
	
	private function generateFieldName($table, $column, $parentId, $fieldId)
	{
		
		if($fieldId == '')
		{
			return $this->FieldName->generateNew($table, $column, $parentId);
		}
		else
		{
			return $this->FieldName->generateExisting($table, $column, $parentId, $fieldId);
		}
		
		
	}
	

	
	private function getLists($projectId)
	{
		return $this->DbOperation->getProjectLists($projectId);
		/*
		$str = 'SELECT disabled, label, list.id FROM list
				INNER JOIN assoc_project_list
					ON assoc_project_list.idlist=list.id
				WHERE assoc_project_list.idproject='.$projectId;			
		$query = $this->db->query($str);
	
		return $query->result_array();
		*/
	
	}
	
	private function getValues($sectionId)
	{
		return $this->DbOperation->getListValues($sectionId);
		/*
		//TODO: n�o t� performatica... t� retornando ds os campos sempre ...
		$str = 'SELECT disabled, label, listvalue.id FROM listvalue
				INNER JOIN assoc_list_listvalue
					ON assoc_list_listvalue.idlistvalue=listvalue.id
				WHERE assoc_list_listvalue.idlist='.$sectionId;			
		$query = $this->db->query($str);
		return $query->result_array();
		*/
	}
}

?>