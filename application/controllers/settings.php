<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class Settings extends CI_Controller {

	public function index($param)//param: projectId
	{
		// variable that holds dynamic form html
		$html = array('content' => '');
	
		$this->load->view('header');
		
	$html['content'] .= '	
		<h1 class="page-header">Settings</h1>
			<div class="row placeholders">
            <div class="col-sm-12">
				<nav class="navbar navbar-default">
				  <div class="container-fluid">
					<div class="navbar-header">
					  <a class="navbar-brand" href="/projects/dynamicforms/index.php/editform/index/0/">
						<span class="glyphicon glyphicon-plus-sign"></span>
					  </a>
					  <!--
					  <a class="navbar-brand" href="/projects/dynamicforms/index.php/editlist/index/0/">
						<span class="glyphicon glyphicon-th-list"></span>
					  </a>
					  -->
					</div>
				  </div>
				</nav>
            </div>
			<br><br>
			</div>	
';
		
		
		$forms = $this->getForms($param);
		foreach ($forms as $s)
		{
			//$s['id']
			$html['content'] .= '
				<div class="panel panel-default">
				  <div class="panel-body">
					'.$s['title'].'     <br><br> <a href="/projects/dynamicforms/index.php/editlist/index/1/">Lists</a>
										<span class="glyphicon glyphicon-chevron-right"></span> Workflow Steps
										<span class="glyphicon glyphicon-chevron-right"></span> Workflow Sequence
										<span class="glyphicon glyphicon-chevron-right"></span> <a href="/projects/dynamicforms/index.php/editform/index/1">Sections & Fields</a>
										<span class="glyphicon glyphicon-chevron-right"></span> Security
				  </div>
				</div>
			';
		}
		

		
		$this->load->view('settings', $html);
		$this->load->view('settingsfooter');
		
	}
		
		
	private function getForms($projectId)
	{
		return array(
						array(
							'formid' => '124', 'title' => 'Título 1'
						),
						array(
							'formid' => '125', 'title' => 'Título 2'
						),
						array(
							'formid' => '126', 'title' => 'Título 3'
						),
						array(
							'formid' => '127', 'title' => 'Título 4'
						),
						array(
							'formid' => '128', 'title' => 'Título 5'
						),
						array(
							'formid' => '129', 'title' => 'Título 6'
						),
						);
	
	}
}

?>