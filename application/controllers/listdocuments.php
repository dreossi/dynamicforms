<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class ListDocuments extends CI_Controller {

	public function index($param)//param: formId
	{

		// variable that holds dynamic form html
		$html = array('content' => '');
	
		$this->load->view('header');
		
		$html['content'] .= '	
		<h1 class="page-header">RDO</h1>
			<div class="row placeholders">
            <div class="col-sm-12">
				<nav class="navbar navbar-default">
				  <div class="container-fluid">
					<div class="navbar-header">
					  <a class="navbar-brand" href="/projects/dynamicforms/index.php/generate/index/123/">
						<span class="glyphicon glyphicon-plus-sign"></span>
					  </a>
					</div>
				  </div>
				</nav>
            </div>
			<br><br>
			</div>	
';
		
		
		$html['content'] .= '
		<table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>ID</th>
                <th>Title</th>
				<th>Status</th>
                <th>Date Created</th>
                <th>Date Modified</th>
            </tr>
        </thead>
		<tbody>
        ';
		/*
		<tfoot>
            <tr>
                <th>Name</th>
                <th>Position</th>
                <th>Office</th>
                <th>Age</th>
                <th>Start date</th>
                <th>Salary</th>
            </tr>
        </tfoot>
		*/
		
		$documents = $this->getDocuments($param);
		foreach ($documents as $s)
		{
			//$s['id']
			$html['content'] .= '
			<tr>
                <td>'.$s['docid'].'</td>
                <td>'.$s['title'].'</td>
				<td>'.$s['status'].'</td>
				<td>'.$s['datecreated'].'</td>
				<td>'.$s['datemodified'].'</td>
            </tr>
			';
		}
		
		$html['content'] .= '
			</tbody>
		</table>';
		
		$this->load->view('listdocuments', $html);
		$this->load->view('footer');
		
	}
		
		
	private function getDocuments($formId)
	{
		return array(
						array(
							'docid' => '123', 'title' => 'Título 1', 'status' => 'aprovado', 'datecreated' => '2014/01/03', 'datemodified' => '2014/01/10'
						),
						array(
							'docid' => '124', 'title' => 'Título 2', 'status' => 'recusado', 'datecreated' => '2014/01/03', 'datemodified' => '2014/01/10'
						),
						array(
							'docid' => '125', 'title' => 'Título 3', 'status' => 'novo', 'datecreated' => '2014/01/03', 'datemodified' => '2014/01/10'
						),
						array(
							'docid' => '126', 'title' => 'Título 4', 'status' => 'recusado', 'datecreated' => '2014/01/03', 'datemodified' => '2014/01/10'
						),
						array(
							'docid' => '128', 'title' => 'Título 5', 'status' => 'aprovado', 'datecreated' => '2014/01/03', 'datemodified' => '2014/01/10'
						),
						array(
							'docid' => '129', 'title' => 'Título 6', 'status' => 'aprovado', 'datecreated' => '2014/01/03', 'datemodified' => '2014/01/10'
						),
						array(
							'docid' => '129', 'title' => 'Título 7', 'status' => 'aprovado', 'datecreated' => '2014/01/03', 'datemodified' => '2014/01/10'
						),
						array(
							'docid' => '130', 'title' => 'Título 8', 'status' => 'aprovado', 'datecreated' => '2014/01/03', 'datemodified' => '2014/01/10'
						),
						array(
							'docid' => '131', 'title' => 'Título 9', 'status' => 'aprovado', 'datecreated' => '2014/01/03', 'datemodified' => '2014/01/10'
						),
						array(
							'docid' => '1233', 'title' => 'Título 96', 'status' => 'aprovado', 'datecreated' => '2014/01/03', 'datemodified' => '2014/01/10'
						),
						array(
							'docid' => '120', 'title' => 'Título 68', 'status' => 'aprovado', 'datecreated' => '2014/01/03', 'datemodified' => '2014/01/10'
						)											
						);
	
	}
}

?>