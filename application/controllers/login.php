<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//include("/application/libraries/php/jformer.php");
require_once("/application/libraries/php/jformer.php");


class Login extends CI_Controller {

	public function index()
	{
		
		$this->load->view('header');

		$this->load->view('login');

		$this->load->view('footer');
		
		// Set the function for a successful form submission
		function onSubmit($formValues) {
			$formValues = $formValues->loginFormPage->loginFormSection;

			if($formValues->username == 'admin' && $formValues->password == '12345') {
				if(!empty($formValues->rememberMe)) {
					$response = array('successPageHtml' => '<p>Login Successful</p><p>We\'ll keep you logged in on this computer.</p>');
				}
				else {
					$response = array('successPageHtml' => '<p>Login Successful</p><p>We won\'t keep you logged in on this computer.</p>');
				}
			}
			else {
				$response = array('failureNoticeHtml' => 'Invalid username or password.', 'failureJs' => "$('#password').val('').focus();");
			}

			return $response;
		}
		
	}
}

?>