<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class ProcessForm extends CI_Controller {

	private $html; 
	
	
	public function index($formType)//param: formType
	{
		// variable that holds dynamic form html
		global $html;
		$html = array('content' => '');
	
		//$this->load->database();
	
		$this->load->model('FieldName');
		
		$this->load->model('DbOperation');
	
		$this->load->view('header');
		
		if($formType == '1')//List
		{
			$this->processList();
		}

		
		/*
		$html['content'] .= 'mostrando informações do formulario recebido<br><br>';
		
		$form_data = $this->input->post();
		$html['content'] .= 'results com post do codeigniter:<br>';
		//$html['content'] .= $this->input->post("12312344125");
		$html['content'] .= '<table>';
		foreach ($form_data as $a => $b)
		{
			$html['content'] .= '
				<tr>
				<td>
				'.$a.'
				</td>
				<td>
				'.$b.'
				</td>
				</tr>
				';
		}
		$html['content'] .= '</table>';
		$html['content'] .= '<br><br>';
		*/
		
		$this->load->view('processform', $html);
		
		$this->load->view('processformfooter');
	}
	
	private function processList()
	{
		global $html;
		
		
		
		$form_data = $this->input->post();
		$projectId = $form_data['projectid'];
		$arrNewFields = array();
		//array_push($arrNewFields, '3'=>'4');
		$currentNewListId = '';
		$arrListInsertIds = array();
		
		
		
		foreach ($form_data as $a => $b)
		{			
			
			
		
			if( $this->checkFieldNameValid($a) )
			{
				//echo '<br><br><br><br><br>chave valida: '.$a.'   valor: '.$b;
				
				$table = $this->getFieldTable($a);
				$column = $this->getFieldColumn($a);
				$id = $this->getFieldId($a);
				$parentId = $this->getParentId($a);
				
				if(($column == DB_COL_DISABLED)&&($b == 'on'))
				{
					$b = '1';
				}
				$data = array(
					   $column => $b
					);
			
				if( $this->checkFieldNameNew($a) )
				{
					if($table == DB_TABLE_LIST)
					{
						if($column == DB_COL_LABEL)
						{
							$arrNewFields[$a] = $b;
						}
					}
					else if($table == DB_TABLE_LISTVALUE)
					{
						if( ($b != '')&&($column == DB_COL_LABEL))
						{
							$arrNewFields[$a] = $b;
						}
					}
					
				}
				else
				{
					// existing field: update query
					//$this->db->where(DB_COL_ID, $id);
					//$this->db->update($table, $data); 
					$this->DbOperation->update($table, $data, DB_COL_ID, $id);
					//echo '<br><br><br><br>'.$a.' = '.$b;
					
				}
			}
			
		}
		
		// Processing new fields
		//echo '<br><br><br><br><br><br><br><pre>';
		//print_r($arrNewFields);
		//print_r($form_data);
		//echo '</pre>';
		//echo '<br><br><br><br><br><br><br>';
		foreach ($arrNewFields as $a => $b)
		{
			//echo $a.'   '.$b.'<br>';
			$table = $this->getFieldTable($a);
			$column = $this->getFieldColumn($a);
			$id = $this->getFieldId($a);
			$parentId = $this->getParentId($a);
			$listId = array();
			
			$disabled = '0';
			if(array_key_exists($table.'_'.DB_COL_DISABLED.'_'.$parentId.'_'.$id, $form_data))
			{
				$disabled = '1';
			}
			$data = array(
				   DB_COL_LABEL => $b ,
				   DB_COL_DISABLED => $disabled
				);
			
			if($table == DB_TABLE_LIST)
			{
				//query para criar lista e relação com projeto
				
				//$this->db->insert(DB_TABLE_LIST, $data); // manter essa linha
				$insertid = $this->DbOperation->insert(DB_TABLE_LIST, $data);//$this->db->insert_id();
				$arrListInsertIds[$id] = $insertid;
				
				// Create insert in assoc table with project
				$data = array(
				   DB_COL_IDPROJECT => $projectId ,
				   DB_COL_IDLIST => $arrListInsertIds[$id]
				);
				//$this->db->insert(DB_TABLE_ASSOCPROJECTLIST, $data); // manter essa linha
				$this->DbOperation->insert(DB_TABLE_ASSOCPROJECTLIST, $data);
				
			}
			else if($table == DB_TABLE_LISTVALUE)
			{
				
				
				if($this->checkIdIsNew($parentId))
				{
					//Lista também é nova, buscar no cache
					
					if(array_key_exists($parentId, $arrListInsertIds))
					{
						//$this->db->insert(DB_TABLE_LISTVALUE, $data); // manter essa linha
						$insertid = $this->DbOperation->insert(DB_TABLE_LISTVALUE, $data);////$this->db->insert_id(); // manter essa linha
						$data = array(
						   DB_COL_IDLIST => $arrListInsertIds[$parentId] ,
						   DB_COL_IDLISTVALUE => $insertid
						);
						//$this->db->insert(DB_TABLE_ASSOCLISTVALUE, $data); // manter essa linha
						$this->DbOperation->insert(DB_TABLE_ASSOCLISTVALUE, $data);
					}
					
				}
				else
				{
					// lsita é existente, somente rodar o insert
					//$this->db->insert(DB_TABLE_LISTVALUE, $data); // manter essa linha
					$insertid = $this->DbOperation->insert(DB_TABLE_LISTVALUE, $data);//$this->db->insert_id();//$this->DbOperation->insert(DB_TABLE_LISTVALUE, $data);// // manter essa linha
					$data = array(
					   DB_COL_IDLIST => $parentId ,
					   DB_COL_IDLISTVALUE => $insertid
					);
					//$this->db->insert(DB_TABLE_ASSOCLISTVALUE, $data); // manter essa linha
					$this->DbOperation->insert(DB_TABLE_ASSOCLISTVALUE, $data);
				}
				
				
			}
			
		}
		
		
		

		
		/*
		$html['content'] .= '<table>';
		foreach ($form_data as $a => $b)
		{
			// remover isso, pois é apenas para teste
			$html['content'] .= '
				<tr>
				<td>
				'.$a.'
				</td>
				<td>
				'.$b.'
				</td>
				</tr>';
			
			// elimina campos vazios
			if($b != '')
			{
				
			}
		}
		$html['content'] .= '</table>';
		$html['content'] .= '<br><br>';
		*/

	}
	

	
	private function getTableFromFieldName($fieldName)
	{
		return $this->FieldName->getTable($fieldName);		
	}
	
	private function checkFieldNameValid($fieldName)
	{
		return $this->FieldName->isValid($fieldName);		
	}
	
	private function checkFieldNameNew($fieldName)
	{
		return $this->FieldName->isNew($fieldName);		
	}
	
	private function getFieldId($fieldName)
	{
		return $this->FieldName->getId($fieldName);		
	}
	
	private function getParentId($fieldName)
	{
		return $this->FieldName->getParentId($fieldName);		
	}
	
	private function getFieldTable($fieldName)
	{
		return $this->FieldName->getTable($fieldName);		
	}
	
	private function getFieldColumn($fieldName)
	{
		return $this->FieldName->getColumn($fieldName);		
	}
	
	private function checkIdIsNew($id)
	{
		return $this->FieldName->idIsNew($id);		
	}
	
	
	
	
	/*
	private function removeN($fieldName)
	{
		return $this->FieldName->removeN($fieldName);		
	}
	*/
}

/*
$query = "SELECT * FROM `table_name` WHERE `submission_id` = '$submission_id'";
$sqlsearch = mysql_query($query);
$resultcount = mysql_numrows($sqlsearch);

if ($resultcount > 0) {
 
    mysql_query("UPDATE `table_name` SET 
                                `name` = '$name',
                                `email` = '$email',
                                `phone` = '$phonenumber',
                                `subject` = '$subject',
                                `message` = '$message'        
                             WHERE `submission_id` = '$submission_id'") 
     or die(mysql_error());
    
} else {

    mysql_query("INSERT INTO `table_name` (submission_id, formID, IP, 
                                                                          name, email, phone, subject, message) 
                               VALUES ('$submission_id', '$formID', '$ip', 
                                                 '$name', '$email', '$phonenumber', '$subject', '$message') ") 
    or die(mysql_error());  

}
*/

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */