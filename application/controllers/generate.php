<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class Generate extends CI_Controller {

	public function index($param)//param: formId
	{
		// variable that holds dynamic form html
		$html = array('content' => '');
		
		$this->load->view('header');
		
		$this->load->database();
		
		
		
		$html['content'] .= '<form data-toggle="validator" role="form" action="/projects/dynamicforms/index.php/processform/index/123/" method="post">';
		
		$html['content'] .= '	
		<h1 class="page-header">RDO</h1>
			<div class="row placeholders">
            <div class="col-sm-12">
				<nav class="navbar navbar-default">
				  <div class="container-fluid">
					<div class="navbar-header">
					  
						
						<div class="btn-group">
						  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
							Route Document <span class="caret"></span>
						  </button>
						  <ul class="dropdown-menu" role="menu">
							<li><a href="#">Action</a></li>
							<li><a href="#">Another action</a></li>
							<li><a href="#">Something else here</a></li>
							<li class="divider"></li>
							<li><a href="#">Separated link</a></li>
						  </ul>
						</div>
						
					</div>
				  </div>
				</nav>
            </div>
			<div class="col-sm-12">
			</div>

			</div>	
';
		
		/* // cria��o de campos din�micos
		$html['content'] .= '
				<div class="wrap">
				<div class="input-group">
					<span class="input-group-btn">
						<button class="btn btn-default" type="button">+</button>
					</span>
					<input type="text" class="form-control" id="inputid" name="inputname" required>  
				</div>
				</div>
		';
		*/
		
		
		// retorna as se��es
		$sections = $this->getSections($param);
		foreach ($sections as $s)
		{
			//echo $row['id'].'<br>';
			//echo $row['label'].'<br>';
			$html['content'] .= '<!-- inicio secao -->
								 
									<div class="panel panel-default">
										<div class="panel-heading">
											<h3 class="panel-title">'.$s['label'].'</h3>
										</div>
										<div class="panel-body">';
			
										
			//			
			// TODO: loop para pegar campos
			//
			$fields = $this->getSectionFields($s['id']);
			foreach($fields as $f)
			{	
				// disabled
				$disabledVar = '';
				//TODO: check permissions to disable
				// required
				$requiredVar = '';
				$requiredText = '';
				if($f['required'] == '1')
				{
					$requiredVar = 'required';
					$requiredText = ' *';
				}
				// width
				$widthVar = '100';
				if($f['width'] != '')
				{
					$widthVar = $f['width'];
				}
				
				$generalErrorMessage = 'Check this information...';
				// condicional para ver qual o campo
				if($f['type'] == '001'){ // texto simples
					
					
					
					$html['content'] .= '
					<div class="form-group">
						<label for="'.$f['id'].'">'.$f['label'].$requiredText.'</label><input type="text" class="form-control" id="'.$f['id'].'" name="'.$f['id'].'" style="width:'.$widthVar.'%" value="'.$f['value'].'" '.$requiredVar.' '.$disabledVar.' data-error="'.$generalErrorMessage.'">
						<span class="help-block with-errors"></span>
					</div>
					';

					

				}
				else if($f['type'] == '002') // dropdown
				{
					$dropDownHtml = '';
					
					// TODO: buscar de tabela com ID e valores de cada entrada da lista, assim se quiserem mudar o label, n�o afeta conteudo legado
					$tempValues = explode('[(&)]', $f['valuelist']);
					foreach($tempValues as $v)
					{
						$dropDownHtml .= '<li id="a'.$f['id'].'"><a href="#" name="'.$f['id'].'">'.$v.'</a></li>';
					}
				
					/*
						<div class="form-group">
							<label for="'.$f['id'].'">'.$f['label'].$requiredText.'</label><br>
							<div class="input-group">
							  <div class="input-group-btn">
								<button type="button" id="a'.$f['id'].'" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false" '.$disabledVar.'><span data-bind="label">Select </span> <span class="caret"></span></button>
								<ul class="dropdown-menu" role="menu">
								  '.$dropDownHtml.'
								</ul>
							  </div><!-- /btn-group -->
							  <input type="text" class="form-control" data-error="'.$generalErrorMessage.'" id="b'.$f['id'].'" name="b'.$f['id'].'" value="'.$f['value'].'" '.$requiredVar.' '.$disabledVar.' readonly>
							  
							</div><!-- /input-group -->
							<span class="help-block with-errors"></span>
						</div>
						*/
						
						$html['content'] .= '
						<div class="form-group">
							<label for="'.$f['id'].'">'.$f['label'].$requiredText.'</label><br>
							<div class="input-group">
								<select class="form-control" id="'.$f['id'].'" '.$disabledVar.' data-error="'.$generalErrorMessage.'" '.$requiredVar.' >
								<option value=""></option>
								<option value="Milk">Fresh Milk</option>
								<option value="Cheese">Old Cheese</option>
								<option value="Bread">Hot Bread</option>
								</select>
							</div>
							<span class="help-block with-errors"></span>
						</div>
					';
					
					/*
					$html['content'] .= '
						<div class="form-group">
							<label for="'.$f['id'].'">'.$f['label'].$requiredText.'</label><br>
							<div class="btn-group btn-input clearfix">
							  <button type="button" id="a'.$f['id'].'" class="btn btn-default dropdown-toggle form-control" data-toggle="dropdown" '.$disabledVar.' >
								<span data-bind="label">'.$f['value'].'</span> <span class="caret"></span>
							  </button>
							  <ul class="dropdown-menu" role="menu">
								  '.$dropDownHtml.'
							  </ul>
							</div>
							<span class="help-block with-errors"></span>
						</div>
						<!-- hidden field for validation -->
						<div class="form-group">
							<input type="input" class="form-control" id="b'.$f['id'].'" value="'.$f['value'].'" '.$requiredVar.' '.$disabledVar.' data-error="'.$generalErrorMessage.'">
							<span class="help-block with-errors"></span>
						</div>
						
					';
					*/
				
					/*
					$html['content'] .= '
						<label for="'.$f['id'].'">'.$f['label'].$requiredText.'</label><br>
						<div class="input-group">
						  <div class="input-group-btn">
							<button type="button" id='.$f['id'].' class="btn btn-inverse dropdown-toggle" data-toggle="dropdown" aria-expanded="false" style="width:'.$widthVar.'%" '.$disabledVar.'>select <span class="caret"></span></button>
							<ul class="dropdown-menu" role="menu">
							  '.$dropDownHtml.'
							</ul>
						  </div><!-- /btn-group -->
						 <!-- <input type="text" id="'.$f['id'].'" class="form-control" style="width:'.$widthVar.'%" value="'.$f['value'].'" '.$requiredVar.' > -->
						</div><!-- /input-group -->';
						*/
						

				}
				else if($f['type'] == '004'){ // text area
					
					if($f['rows'] == '')
					{
						$f['rows'] = '3';
					}
				
					$html['content'] .= '
					<div class="form-group">
						<label for="'.$f['id'].'">'.$f['label'].$requiredText.'</label><textarea type="text" class="form-control" id="'.$f['id'].'" name="'.$f['id'].'" style="width:'.$widthVar.'%" rows="'.$f['rows'].'" '.$requiredVar.' '.$disabledVar.' data-error="'.$generalErrorMessage.'">'.$f['value'].'</textarea>
						<span class="help-block with-errors"></span>
					</div>';
				}
			}
			
			$html['content'] .= '		
									</div>
								</div>
								 <!-- fim secao -->';
			
		}
		
		$html['content'] .= '<button type="submit" class="btn btn-primary">Submit</button>
							 </form>';
		$this->load->view('generate', $html);
		
		
		/*
			<form role="form">
			  <!-- -->
			  <div class="form-group">
				<div class="panel panel-default">
				  <div class="panel-heading">
					<h3 class="panel-title">Panel title</h3>
				  </div>
				  <div class="panel-body">
				  <!-- -->
					<label for="exampleInputEmail1">Email address</label>
					<input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
				  <!-- -->
				  </div>
				</div>
			  </div>
			  <!-- -->
			  <div class="form-group">
				<div class="panel panel-default">
				  <div class="panel-heading">
					<h3 class="panel-title">Panel title</h3>
				  </div>
				  <div class="panel-body">
				  <!-- -->
					<label for="exampleInputPassword1">Password</label>
					<input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
			  </div>
			  </div>
			  </div>
			  <div class="checkbox">
				<label>
				  <input type="checkbox"> Check me out
				</label>
			  </div>
			  <button type="submit" class="btn btn-default">Submit</button>
			</form>

		*/

		$this->load->view('footer');
		
		
		
	}
	
	private function getSections($formId)
	{
	
		/*
		// query
		$query = $this->db->query('SELECT * FROM bitauth_users');

		echo 'printando resutlados query <br>';
		foreach ($query->result_array() as $row)
		{
			echo $row['username'].'<br>';
			echo $row['password'].'<br>';
		}
		*/
		$arr = array(
						array(
							'id' => '123', 'label' => 'Secao 1'
						),
						
						array(
							'id' => '122', 'label' => 'Secao 1.1'
						),
						array(
							'id' => '124', 'label' => 'Secao 2'
						)	
											
						);
					
		
					
		return $arr;
	}
	
	private function getSectionFields($sectionId)
	{
		return array(
						array(
							'sectionid' => '123', 'id' => $sectionId.'12344125', 'type' => '001', 'label' => 'campo de texto 1', 'required' => '1', 'tip' => 'tip', 'disabled' => '1', 'value' => 'valor do banco1', 'width' => '10'
						),
						array(
							'sectionid' => '123', 'id' => $sectionId.'12344115', 'type' => '003', 'label' => 'campo de data 1', 'required' => '0', 'tip' => 'tip', 'disabled' => '1', 'value' => '', 'width' => '20'
						),
						array(
							'sectionid' => '123', 'id' => $sectionId.'12344126', 'type' => '001', 'label' => 'campo de texto 11', 'required' => '1', 'tip' => 'tip', 'disabled' => '0', 'value' => 'valor da lista', 'width' => '30'
						),
						array(
							'sectionid' => '123', 'id' => $sectionId.'12344127', 'type' => '002', 'label' => 'drop down 1', 'required' => '1', 'tip' => 'tip', 'disabled' => '1', 'value' => '', 'valuelist' => 'valor1[(&)]valor2', 'width' => '50'
						),
						array(
							'sectionid' => '123', 'id' => $sectionId.'123441279', 'type' => '004', 'label' => 'textarea 1', 'required' => '1', 'tip' => 'tip', 'disabled' => '1', 'value' => 'campo preenchido', 'valuelist' => 'valor1[(&)]valor2', 'width' => '100', 'rows' => '10'
						),
						array(
							'sectionid' => '124', 'id' => $sectionId.'1234', 'type' => '001', 'label' => 'campo de texto 2', 'required' => '1', 'tip' => 'tip', 'disabled' => '0', 'value' => 'valor do banco3 banco carro moto banco', 'width' => '100'
						)						
						);
	}
}

?>