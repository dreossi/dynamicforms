<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class DbOperation extends CI_Model {


// TODO: add protection to sql injection and special chars from codeigniter???
	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		
		$this->load->database();
    }
    
	public function update($table, $data, $column, $condition)
	{
		$this->db->where($column, $condition);
		$this->db->update($table, $data); 
	}
	
	public function insert($table, $data)
	{
		$this->db->insert($table, $data);
		$insertid = $this->db->insert_id();
		return $insertid;
	}
	
	public function getForm($formId)
	{
		$str = 'SELECT disabled, label, id, status FROM form
				WHERE id='.$formId;			
		$query = $this->db->query($str);
		return $query->result_array();
	}
	
	public function getFormSections($formId)
	{	
		$str = "SELECT disabled, label, section.id, status, `order` FROM section
				INNER JOIN assoc_form_section
					ON assoc_form_section.idsection=section.id
				WHERE assoc_form_section.idform=".$formId;			
		$query = $this->db->query($str);
		return $query->result_array();
	}
	
	public function getSectionFields($sectionId)
	{	
		$str = 'SELECT disabled, label, component.id, status, `order`, required, type, list, rows FROM component
				INNER JOIN assoc_section_component
					ON assoc_section_component.idcomponent=component.id
				WHERE assoc_section_component.idsection='.$sectionId;			
		$query = $this->db->query($str);
		return $query->result_array();
	}
	
	public function getProjectLists($projectId)
	{
		$str = 'SELECT disabled, label, list.id FROM list
				INNER JOIN assoc_project_list
					ON assoc_project_list.idlist=list.id
				WHERE assoc_project_list.idproject='.$projectId;			
		$query = $this->db->query($str);
		return $query->result_array();
	}
	
	public function getListValues($listId)
	{
		//TODO: n�o t� performatica... t� retornando todos os campos sempre ...
		$str = 'SELECT disabled, label, listvalue.id FROM listvalue
				INNER JOIN assoc_list_listvalue
					ON assoc_list_listvalue.idlistvalue=listvalue.id
				WHERE assoc_list_listvalue.idlist='.$listId;			
		$query = $this->db->query($str);
		return $query->result_array();
	}
	
	
	
}

?>