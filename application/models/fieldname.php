<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class FieldName extends CI_Model {

	// class for setting and getting field names
	
	private $counter = 0;


	public function index()
	{
		return;
	}
	
	public function generateNew($table, $column, $parentId)
	{
		global $counter;
		/* regra: coluna: 
					parentId: 
					n: for new
					random number
					*/
		$counter = $counter + 1;
		$str = $table.'_'.$column.'_'.$parentId.'_n'.$counter;
		return $str;
	}
	
	public function generateExisting($table, $column, $parentId, $fieldid)
	{
		// regra: coluna_parent_field
		
		$str = $table.'_'.$column.'_'.$parentId.'_'.$fieldid;
		return $str;		
	}
	
	public function isNew($nameStr)
	{
		// TODO: aplicar regex
		/*
		$arr = explode('_' ,$nameStr);
		if(count($arr) == 4)
		{
			if(strlen($arr[3]) > 0)
			{
				$fistChar = substr($arr[3], 0 ,1);
				if($fistChar == 'n')
				{
					return TRUE;
				}
			}
		}
		return FALSE;
		*/
		
		$pattern = '/[a-z]+[_][a-z]+[_][n]{0,1}[0-9]+[_][n]{1}[0-9]+/';
		if( preg_match($pattern, $nameStr) )
		{
			return TRUE;
		}
		return FALSE;
	}
	
	public function idIsNew($nameStr)
	{
		$pattern = '/[n]{1}[0-9]+/';
		if( preg_match($pattern, $nameStr) )
		{
			return TRUE;
		}
		return FALSE;
	}
	
	public function getId($nameStr)
	{
		$out = '';
		if( $this->isValid($nameStr) )
		{
			$arr = explode('_' ,$nameStr);
			$out = $arr[3];
		}
		return $out;
	}
	
	public function getParentId($nameStr)
	{
		$out = '';
		if( $this->isValid($nameStr) )
		{
			$arr = explode('_' ,$nameStr);
			$out = $arr[2];
		}
		return $out;
	}
	
	public function getTable($nameStr)
	{
		$out = '';
		if( $this->isValid($nameStr) )
		{
			$arr = explode('_' ,$nameStr);
			$out = $arr[0];
		}
		return $out;
	}
	
	public function getColumn($nameStr)
	{
		$out = '';
		if( $this->isValid($nameStr) )
		{
			$arr = explode('_' ,$nameStr);
			$out = $arr[1];
		}
		return $out;
	}
	
	public function isValid($nameStr)
	{
		$pattern = '/[a-z]+[_][a-z]+[_][n]{0,1}[0-9]+[_][n]{0,1}[0-9]+/';
		if( preg_match($pattern, $nameStr) )
		{
			return TRUE;
		}
		return FALSE;
	}
	
	
	
	/*
	public function removeN($nameStr)
	{
		$out = '';
		if( $this->isValid($nameStr) )
		{
			$arr = explode('_' ,$nameStr);
			$fix = substr($arr[3], 1);
			$out = $arr[0].'_'. $arr[1].'_'.$arr[2].'_'.$fix;
		}
		return $out;
	}
	*/
	
}

?>