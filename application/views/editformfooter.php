

			</div>
		
		
		
	</div>
		
	
</div>






<script src="/js/jquery-1.11.1.min.js"></script>
<script src="/js/bootstrap.min.js"></script>
<script src="/js/validator.min.js"></script>
<script src="/js/jquery.dataTables.min.js"></script>




<script type='text/javascript'>

var nextSection = 1000000;
var nextField = 2000000;


function addField(objId){

	var formId = document.getElementById('formid').value;
	
	//var addto = "#objId" + nextField;
	var addto = "#" + objId;
		
		//var newIn = '<input autocomplete="off" class="input form-control" id="field' + nextField + '" name="field' + nextField + '" type="text">';
		var newIn = '<div id="field' + nextField + '">\
												<div class="panel panel-default">\
												  <div class="panel-body">\
													<div class="row">\
														<div class="col-xs-6 col-md-3">\
															<div class="form-group">\
																<label>Field Type:</label><br>\
																<div class="input-group">\
																	<select name="component_type_'+objId+'_n'+nextField+'" class="form-control">\
																		<option value=""></option>\
																		<option value="001">Single Line Text</option>\
																		<option value="002">DropDown List</option>\
																		<option value="004">MultLine Text</option>\
																	</select>\
																</div>\
															</div>\
														</div>\
														<div class="col-xs-6 col-md-3">\
															<div class="form-group">\
																<div class="input-group">\
																	<label>Label:</label><br>\
																	<input type="text" class="form-control" name="component_label_'+objId+'_n'+nextField+'">\
																</div>\
															</div>\
															<div class="form-group">\
																<div class="input-group">\
																	<label>List:</label><br>\
																	<select name="component_list_'+objId+'_n'+nextField+'" class="form-control" disabled>\
																		<option value=""></option>\
																		<option value="001">list 1</option>\
																		<option value="002">list 2</option>\
																		<option value="004">list 3</option>\
																	</select>\
																</div>\
															</div>\
														</div>\
														<div class="col-xs-6 col-md-3">\
															<div class="form-group">\
																<div class="input-group">\
																	<label>Order:</label><br>\
																	<input type="text" class="form-control" name="component_order_'+objId+'_n'+nextField+'" style="width:50%">\
																</div>\
															</div>\
															<div class="form-group">\
																<div class="input-group">\
																	<label>Rows:</label><br>\
																	<input type="text" class="form-control" name="component_rows_'+objId+'_n'+nextField+'" style="width:50%" disabled>\
																</div>\
															</div>\
														</div>\
														<div class="col-xs-6 col-md-3">\
															<div class="form-group">\
																<div class="input-group">\
																	<label>Required:</label><br>\
																	<input name="component_required_'+objId+'_n'+nextField+'" type="checkbox">\
																</div>\
															</div>\
															<div class="form-group">\
																<div class="input-group">\
																	<label>Disabled:</label><br>\
																	<input name="component_disabled_'+objId+'_n'+nextField+'" type="checkbox">\
																</div>\
															</div>\
														</div>\
													</div>\
												  </div>\
												</div>\
									</div>';
        var newInput = $(newIn);
		$(addto).before(newInput);
		$("#field" + nextField).attr('data-source',$(addto).attr('data-source'));
		nextField = nextField + 1;
        //$("#count").val(nextField);
	
	}

$(document).ready(function(){
	
	$(".add-section").click(function(e){
        e.preventDefault();
		
		//var $buttonId = $(this).prop('id');
		//document.getElementById('title').value=$buttonId ;
		var formId = document.getElementById('formid').value;

		var addto = "#section";// + nextSection;
		nextSection = nextSection + 1;
		var newIn = '<div id="section' + nextSection + '">\
						<div class="panel panel-default">\
											<div class="panel-heading">\
												<h3 class="panel-title">\
													<div class="row">\
														<div class="col-xs-9 col-md-6">\
															<div class="form-group">\
																<label>Section Title</label>\
																<input type="text" class="form-control" name="section_label_'+formId+'_n'+nextSection+'">\
															</div>\
														</div>\
														<div class="col-xs-6 col-md-3">\
															<div class="form-group">\
																<label>Section Order</label>\
																<input type="text" class="form-control" name="section_order_'+formId+'_n'+nextSection+'">\
															</div>\
														</div>\
														<div class="col-xs-6 col-md-3">\
															<div class="form-group">\
																<label>Disabled:</label><br>\
																<input name="section_disabled_'+formId+'_n'+nextSection+'" type="checkbox">\
															</div>\
														</div>\
													</div>\
												</h3>\
											</div>\
											<div class="panel-body">\
												<div id="field0">\
												</div>\
												<div id="section' + nextSection + '">\
													<button name="n'+ nextSection +'" id="n'+ nextSection +'" type="button" class="btn btn-default add-field" aria-label="Left Align" onclick="addField(this.id);">\
													<span class="glyphicon glyphicon-plus-sign" aria-hidden="true"> Add Field</span>\
													</button>\
												</div>\
											</div>\
										</div>\
									</div>';
        var newInput = $(newIn);
		$(addto).before(newInput);
		$("#section" + nextSection).attr('data-source',$(addto).attr('data-source'));
        //$("#count").val(nextSection);
		
		
    });
    

    
});

/*
<div class="container">
	<div class="row">
		<input type="hidden" name="count" value="1" />
        <div class="control-group" id="fields">
            <label class="control-label" for="field1">Nice Multiple Form Fields</label>
            <div class="controls" id="profs"> 
                <form class="input-append">
                    <div id="field"><input autocomplete="off" class="input" id="field1" name="prof1" type="text" placeholder="Type something" data-items="8"/><button id="b1" class="btn add-more" type="button">+</button></div>
                </form>
            <br>
            <small>Press + to add another form field :)</small>
            </div>
        </div>
	</div>
</div>
*/

</script>


</body>
</html>