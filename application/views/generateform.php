<?php

		require_once("/application/libraries/php/jformer.php");

		// dados da secao e campo: a vir do banco de dados
		$formLabel = 'Titulo Form';
		
		
		//query recebendo info do formulario
		//$arrSectionsId = array('123', '124');
		//$arrSectionsInfo = array('label' => '123');
		
		$arrSectionsInfo = array(
						array(
							'id' => '123', 'label' => 'Secao 1'
						),
						array(
							'id' => '124', 'label' => 'Secao 2'
						)						
						);
		
		
		// query recebendo as se��es do formulario
		//$arrComponentsId = array ('12344125', '1234442332', '12334445');
		
		// query para pegar os campos de cada se��o
		$arrComponentInfo = array(
						array(
							'sectionid' => '123', 'id' => '12344125', 'type' => '001', 'label' => 'campo de texto 1', 'required' => 'required', 'tip' => 'tip', 'disabled' => 'true', 'initialvalue' => 'valor do banco1', 'width' => 'short'
						),
						array(
							'sectionid' => '123', 'id' => '12344115', 'type' => '003', 'label' => 'campo de data 1', 'required' => 'required', 'tip' => 'tip', 'disabled' => 'true', 'initialvalue' => '', 'width' => ''
						),
						array(
							'sectionid' => '123', 'id' => '12344126', 'type' => '001', 'label' => 'campo de texto 11', 'required' => 'required', 'tip' => 'tip', 'disabled' => '', 'initialvalue' => 'valor da lista', 'width' => 'medium'
						),
						array(
							'sectionid' => '123', 'id' => '12344127', 'type' => '002', 'label' => 'drop down 1', 'required' => 'required', 'tip' => 'tip', 'disabled' => 'true', 'initialvalue' => 'a', 'valuelist' => 'valor1[(&)]valor2', 'width' => ''
						),
						array(
							'sectionid' => '124', 'id' => '1234', 'type' => '001', 'label' => 'campo de texto 2', 'required' => '', 'tip' => 'tip', 'disabled' => '', 'initialvalue' => 'valor do banco3 banco carro moto banco', 'width' => 'longest'
						)						
						);
		
			
		
		
		/*
		$sectionLabel = 'Titulo Secao 1';
		$fieldId = '000994993483858235';
		$fieldLabel = 'Username';
		$fieldRequired = 'required';
		$fieldTip = 'The demo login is admin';
		*/
		
		

		// Create the form
		$login = new JFormer('loginForm', array(
			'submitButtonText' => 'Send',
			'submitProcessingButtonText' => 'Processing...',
		));

		// Create the form page
		$jFormPage1 = new JFormPage($login->id.'Page', array(
			'title' => $formLabel,
		));

		

		// montagem do array de componentes
		
		for ($i = 0; $i < count($arrSectionsInfo); $i++)
		{
			// Create the form section
			//$temp = $arrSectionsInfo[$i]['id'];
			$jFormSection1[$i] = new JFormSection($login->id.$arrSectionsInfo[$i]['id'], array(
				'title' => $arrSectionsInfo[$i]['label'],
			));
		
		
			
			$arrComponentTemp = array();
			//$array[$i]
			//echo(count($arrComponentInfo).'<br>');
			for ($j = 0; $j < count($arrComponentInfo); $j++)
			{	
				//echo('iniciando i='.$i.'<br>');
				//echo('comparando sectionid e componentsectionid: '.$arrSectionsInfo[$i]['id'].'  '.$arrComponentInfo[$j]['sectionid'].'<br>');
				if($arrSectionsInfo[$i]['id'] == $arrComponentInfo[$j]['sectionid'])
				{
					//echo('a');
					//echo('count do arrComponentInfo '.$j.'] = '.count($arrComponentInfo[$j]).'<br>');
					//echo('vlor do J: '.$j.'   valor do conteudo: '.$arrComponentInfo[$i][$j].'<br>');
					
					//TODO: se o usuario nao tiver permissao, deve-se trocar o campo para disabled
					switch($arrComponentInfo[$j]['type'])
					{
						case '001': //JFormComponentSingleLineText
						{
							//echo('criando componente  '.$i.'  '.$j.'<br>');
							$arrComponentTemp[$j] = new JFormComponentSingleLineText(
															$arrComponentInfo[$j]['id'], 
															$arrComponentInfo[$j]['label'], 
															array('validationOptions' => array($arrComponentInfo[$j]['required'], $arrComponentInfo[$j]['id']),'tip' => $arrComponentInfo[$j]['tip'], 'disabled' => $arrComponentInfo[$j]['disabled'], 'initialValue' => $arrComponentInfo[$j]['initialvalue'], 'width' => $arrComponentInfo[$j]['width'])
							);
							break;
						};
						case '002': //JFormComponentDropDown
						{
							//echo('criando dropdown  '.$i.'  '.$j.'<br>');
							$arrDropValues = array();
							if($arrComponentInfo[$j]['disabled'] == 'true')
							{
								array_push($arrDropValues, array('value' => $arrComponentInfo[$j]['initialvalue'], 'label' => $arrComponentInfo[$j]['initialvalue']));
							}
							else
							{
								// TODO: mesmo estando disabled precisa trazer o initialvalue antes
								array_push($arrDropValues, array('value' => $arrComponentInfo[$j]['initialvalue'], 'label' => $arrComponentInfo[$j]['initialvalue']));
								$tempValues = explode('[(&)]', $arrComponentInfo[$j]['valuelist']);
								foreach ($tempValues as $k)
								{
									array_push($arrDropValues, array('value' => $k, 'label' => $k));
								}
								//array_push($arrDropValues, $tempValues);
							}
							$arrComponentTemp[$j] = new JFormComponentDropDown(
															$arrComponentInfo[$j]['id'], 
															$arrComponentInfo[$j]['label'], 
															//array('validationOptions' => array($arrComponentInfo[$j]['required'], $arrComponentInfo[$j]['id']),'tip' => $arrComponentInfo[$j]['tip'],)
															$arrDropValues, //valores
															array('validationOptions' => $arrComponentInfo[$j]['required'], 'tip' => $arrComponentInfo[$j]['tip'], 'disabled' => $arrComponentInfo[$j]['disabled']) // validacoes
							);
							break;
						};
						case '003': //JFormComponentDate
						{
							//echo('criando componente  '.$i.'  '.$j.'<br>');
							$arrComponentTemp[$j] = new JFormComponentDate(
															$arrComponentInfo[$j]['id'], 
															$arrComponentInfo[$j]['label'], 
															array('validationOptions' => array($arrComponentInfo[$j]['required']),'tip' => $arrComponentInfo[$j]['tip'], 'disabled' => $arrComponentInfo[$j]['disabled'], 'initialValue' => $arrComponentInfo[$j]['initialvalue'])
							);
							break;
						};
					}
				}
			
			}
			
			$jFormSection1[$i]->addJFormComponentArray($arrComponentTemp);
			
			// Add the section to the page
			$jFormPage1->addJFormSection($jFormSection1[$i]);
			
		}
		
		// Add components to the section
		/*
		$jFormSection1->addJFormComponentArray(array(
			new JFormComponentSingleLineText($arrComponentInfo['id'], $fieldLabel, array(
				'validationOptions' => array($fieldRequired, $arrComponentInfo['id']),
				'tip' => $fieldTip,
			)),

		));
		*/

		

		// Add the page to the form
		$login->addJFormPage($jFormPage1);

		

		// Process any request to the form
		$login->processRequest();



?>