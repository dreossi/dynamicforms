        </div>
    </div>
    
 
</div>



        <!-- bottom of the page -->
        <div class="footer">
            <div class="footer-menu">
                <div class="container">
                    <div class="row bcit50">
                        <ul class="nav nav-pills">
                            
<li class="{active}"><a href="http://www.codeigniter.com/help">Policies</a></li>

<li class="{active}"><a href="http://www.codeigniter.com/help/legal">The Fine Print</a></li>

<li class="{active}"><a href="http://www.codeigniter.com/help/about">About</a></li>



                        </ul>
                    </div>
                </div>
            </div>
            <div class="footer-copyright">
                <div class="container">
                    <div class="row">
                        <p class="text-center">CodeIgniter was created by <a href="http://www.ellislab.com/" "="">EllisLab</a>, 
                            and is now a project of the <a href="http://www.bcit.ca/cas/computing/">British Columbia Institute of Technology</a>
                            <a href="mailto:jim_parry@bcit.ca"><span class="glyphicon glyphicon-envelope"></span></a></p>
                    </div>
                </div>
            </div>
        </div>

<!--
        <script src="/js/jquery-1.11.1.min.js"></script>
        <script src="/js/bootstrap.min.js"></script>
    -->


</body></html>